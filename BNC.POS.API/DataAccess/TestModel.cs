﻿using BNC.POS.API.BussinessHelpers;
using BNC.POS.API.DbConnection;
using BNC.POS.API.DbHelpers;
using BNC.POS.API.DTO_s;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BNC.POS.API.Models
{
    public class TestModel : ITestModel
    {
        public SqlDataReader GetCountryDetailsById(TestDTO obj)
        {
            try
            {
                var para = new[] {
                    new SqlParameter("@CountryId",obj.Id)
                };
                return DbConnector.ExecuteReader("uspGetCountryById", para);
            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetCountryDetailsById");
                return null;
            }
        }

        public SqlDataReader GetCountryList(TestDTO obj)
        {
            try
            {
                return DbConnector.ExecuteReader("upGetCountry", null);
            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> InsertUpdateAccount");
                return null;
            }
        }

        public SqlDataReader InsertUpdateCountry(TestDTO obj)
        {
            try
            {
                var para = new[] {
                    new SqlParameter("@Id",obj.Id),
                    new SqlParameter("@Name",obj.Name) ,
                    new SqlParameter("@FlagId",obj.FlagId)
                };
                return DbConnector.ExecuteReader("uspIsertUpdateCountry", para);
            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> InsertUpdateAccount");
                return null;
            }
        }

        public SqlDataReader GetProjectType(ProjectDetails_DTO obj)
        {
            try
            {
                var parameter = new[]
               {
                    new SqlParameter("@Emp_No",obj.Emp_No)
                };
                return DbConnector.ExecuteReader("GetProjectTypesByUserName", parameter);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public SqlDataReader GetPortfolioByEmployee(PortfolioDTO obj)
        {
            try
            {
                var parameter = new[]
               {
                    new SqlParameter("@EmployeeId",obj.Emp_No)
                };
                return DbConnector.ExecuteReader("Portfolio_GetOnlyPortfolioByEmployeeId", parameter);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public SqlDataReader GetEmployeeByComp_No(Employee_DTO obj)
        {
            try
            {
                var parameter = new[]
               {
                    new SqlParameter("@Comp_No",obj.Emp_Comp_No),
                    new SqlParameter("@Portfolio_id",obj.Portfolio_ID)
                };
                return DbConnector.ExecuteReader("GetEmployeesBy_CompNo", parameter);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public SqlDataReader GetProjects(ProjectDTO obj)
        {
            try
            {
                var parameter = new[]
                {
                    new SqlParameter("@ProjectType_ID",obj.ProjectType_ID),
                };
                return DbConnector.ExecuteReader("sp_GetProjects", parameter);

            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetProjects");
                return null;

            }
        }
        public SqlDataReader GetProjectDetails() //
        {
            try
            {

                return DbConnector.ExecuteReader("Sp_GetProjects_EPDB", null);

            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetProjectDetails");
                return null;

            }
        }


        public SqlDataReader GetProjectsBy_PortfolioId(PortfolioDTO obj)
        {
            try
            {
                var parameter = new[]
                {
                    new SqlParameter("@PortfolioId",obj.Portfolio_ID)

                };
                return DbConnector.ExecuteReader("Portolio_GetProjectsByPortfolioId", parameter);

            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetProjectsBy_PortfolioId");
                return null;

            }
        }

        public SqlDataReader GetProjectDetailsByUserName(ProjectDetails_DTO obj)
        {
            try
            {
                var parameter = new[]
                {
                    new SqlParameter("@CompNo",obj.Emp_Comp_No),
                    new SqlParameter("@EmpNo",obj.Emp_No),
                    new SqlParameter("@ProjectType",obj.Exec_BlockName),
                    new SqlParameter("@Status",obj.Status),
                    new SqlParameter("@SystemRole",obj.SystemRole)
                   // new SqlParameter("@SearchText",obj.SearchText),
                   // new SqlParameter("@PageNumber",obj.PageNumber),
                    //new SqlParameter("@RowsOfPage",10)
                     
   
                    //new SqlParameter("@ProjectCodes",obj.Project_Code)

                };
                return DbConnector.ExecuteReader("[dbo].GetPortfolioProjects", parameter);

            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetProjectDetailsByUserName");
                return null;

            }
        }

        public SqlDataReader GetEmployeById(Employee_DTO obj)
        {
            try
            {
                var parameter = new[]
                {
                    new SqlParameter("@Emp_No",obj.Emp_No),
                };
                return DbConnector.ExecuteReader("Sp_GetEmployeesById", parameter);

            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetProjects");
                return null;

            }
        }
        public SqlDataReader GetPortfolio() //
        {
            try
            {

                return DbConnector.ExecuteReader("Sp_GetPortfolio_ByEmpNo", null);

            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetPortfolio");
                return null;

            }
        }
        public SqlDataReader GetEmployees() //
        {
            try
            {

                return DbConnector.ExecuteReader("Sp_GetEmployees", null);

            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetProjectDetails");
                return null;

            }
        }
        public SqlDataReader GetCompanies() //
        {
            try
            {

                return DbConnector.ExecuteReader("SP_Get_All_Companies", null);

            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetCompanies");
                return null;

            }
        }

        public SqlDataReader InsertUpdatePortfolio(PortfolioDTO obj)
        {
            try
            {
                var parameter = new[]
                {
                        new SqlParameter("@Portfolio_Id",obj.Portfolio_ID),
                        new SqlParameter("@CreatedBy",obj.Created_By),
                         new SqlParameter("@ModifiedBy",obj.Modified_By),
                        new SqlParameter("@PortfolioName",obj.Portfolio_Name),
                        new SqlParameter("@Status",obj.Status),
                        new SqlParameter("@SelectedProjects",obj.SelectedProjectsJson)
                };
                return DbConnector.ExecuteReader("Sp_InsertUpdate_Portfolio", parameter);
            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> InsertUpdatePortfolio");
                DataModelExceptionUtility.NotifySystemOperators(ex);
                obj.message = ex.ToString();
                return null;
            }
        }

        public SqlDataReader InsertUpdateSharePortfolio(SharePortfolio_DTO obj)
        {
            try
            {
                var parameter = new[]
                {
                        new SqlParameter("@Portfolio_Id",obj.Portfolio_ID),
                        new SqlParameter("@EmployeeId",obj.EmployeeId),
                         new SqlParameter("@CompanyId",obj.CompanyId),
                        new SqlParameter("@Preference",obj.Preference),
                        new SqlParameter("@Shared_By",obj.Shared_By)
                         //new SqlParameter("@FlagId ",obj.FlagId)
                  
                };
                return DbConnector.ExecuteReader("InsertSharePortfolio", parameter);
            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> InsertUpdateSharePortfolio");
                DataModelExceptionUtility.NotifySystemOperators(ex);
                obj.Message = ex.ToString();
                return null;
            }
        }

        public SqlDataReader GetLoginDetails(LoginDTO obj)
        {
            try
            {
                var parameters = new[]
                        {
                        new SqlParameter("@username",obj.UserName)
                        //new SqlParameter("@password",obj.Password)
                        };
                return DbConnector.ExecuteReader("Usp_Login", parameters);
            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetLoginDetails");
                DataModelExceptionUtility.NotifySystemOperators(ex);
                return null;
            }

        }

        public SqlDataReader GetUserDetails(LoginDTO obj)
        {
            try
            {
                var parameters = new[]
                        {
                        new SqlParameter("@username",obj.UserName)
                       
                        };
                return DbConnector.ExecuteReader("Usp_UserDetailsByUserName", parameters);
            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetUserDetails");
                DataModelExceptionUtility.NotifySystemOperators(ex);
                return null;
            }

        }

        public SqlDataReader GetPortfolioSharePreferences_Json(PortfolioDTO obj)
        {
            try
            {
                var parameters = new[]
                        {
                        new SqlParameter("@EmployeeId",obj.EmployeeId),
                        new SqlParameter("@PortfolioId",obj.Portfolio_ID)

                        };
                return DbConnector.ExecuteReader("uspGetSharedPortfolioDetails", parameters);
            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetUserDetails");
                DataModelExceptionUtility.NotifySystemOperators(ex);
                return null;
            }

        }

        public SqlDataReader GetPreferencesByEmployeeId(SharePortfolio_DTO obj)
        {
            try
            {
                var parameters = new[]
                        {
                        new SqlParameter("@EmployeeId",obj.EmployeeId),
                        new SqlParameter("@PortfolioId",obj.Portfolio_ID)

                        };
                return DbConnector.ExecuteReader("Portfolio_GetPreferencesByEmployeeId", parameters);
            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetUserDetails");
                DataModelExceptionUtility.NotifySystemOperators(ex);
                return null;
            }

        }
        //GetShareDetailsByPortfolio_ID SqlDataReader GetStatus(StatusDTO obj);
        public SqlDataReader GetShareDetailsByPortfolio_ID(SharePortfolio_DTO obj)
        {
            try
            {
                var parameters = new[]
                        {
                        new SqlParameter("@PortfolioId",obj.Portfolio_ID)
                        };
                return DbConnector.ExecuteReader("Portfolio_GetShareDetailsBy_PID", parameters);
            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetUserDetails");
                DataModelExceptionUtility.NotifySystemOperators(ex);
                return null;
            }

        }
        public SqlDataReader GetStatus(StatusDTO obj)
        {
            try
            {
                var parameters = new[]
                        {
                        new SqlParameter("@PortfolioId",obj.Portfolio_ID)
                        };
                return DbConnector.ExecuteReader("GetStatus_OnDeadlineDate", parameters);
            }
            catch (Exception ex)
            {
                DataModelExceptionUtility.LogException(ex, "TestModel -> GetUserDetails");
                DataModelExceptionUtility.NotifySystemOperators(ex);
                return null;
            }

        }

    }

    public interface ITestModel
    {
        SqlDataReader GetCountryList(TestDTO obj);
        SqlDataReader InsertUpdateCountry(TestDTO obj);
        SqlDataReader GetCountryDetailsById(TestDTO obj);
        SqlDataReader GetProjectType(ProjectDetails_DTO obj);
        SqlDataReader GetPortfolioByEmployee(PortfolioDTO obj);

        SqlDataReader GetEmployeeByComp_No(Employee_DTO obj);
        SqlDataReader InsertUpdatePortfolio(PortfolioDTO obj);

        SqlDataReader InsertUpdateSharePortfolio(SharePortfolio_DTO obj);

        SqlDataReader GetProjects(ProjectDTO obj);

        SqlDataReader GetProjectDetails();
        SqlDataReader GetProjectsBy_PortfolioId(PortfolioDTO obj);
        SqlDataReader GetProjectDetailsByUserName(ProjectDetails_DTO obj);
        SqlDataReader GetEmployeById(Employee_DTO obj);

        SqlDataReader GetEmployees();
        SqlDataReader GetPortfolio();

        SqlDataReader GetCompanies();

        SqlDataReader GetLoginDetails(LoginDTO obj);
        SqlDataReader GetUserDetails(LoginDTO obj);

        SqlDataReader GetPreferencesByEmployeeId(SharePortfolio_DTO obj);

        SqlDataReader GetShareDetailsByPortfolio_ID(SharePortfolio_DTO obj);

        SqlDataReader GetPortfolioSharePreferences_Json(PortfolioDTO obj);
        SqlDataReader GetStatus(StatusDTO obj);
    }
}