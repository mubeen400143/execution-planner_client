﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNC.POS.API.DTO_s
{
    public class ProjectDetails_DTO
    {
        public int id { get; set; }
        public int Exec_BlockNo { get; set; }
        public string Exec_BlockName { get; set; }
        public string Project_Code { get; set; }
        public string Project_Block { get; set; }
        public string Project_Name { get; set; }
        public string Project_Description { get; set; }
        public int Duration { get; set; }
        public DateTime Project_EndDate { get; set; }
        public DateTime DeadLine { get; set; }
        public string Status { get; set; }
        public string Emp_First_Name { get; set; }
        public string Emp_No { get; set; }
        public string Emp_Comp_No { get; set; }
        public string Team_Res { get; set; }
        public string Team_Autho { get; set; }
        public string Team_Coor { get; set; }
        public string Team_Informer { get; set; }
        public string Team_Support { get; set; }
        public string Project_Owner { get; set; }
        public string Project_Auditor { get; set; }
        public string DurationTime { get; set; }
        public int Project_Duration { get; set; }
        public string Remarks { get; set; }
        public string Com_Image { get; set; }
        public string Com_DARFinal_TopImg { get; set; }
        public string Emp_SystemRole { get; set; }
        public string SystemRole { get; set; }
        public string ProjectType { get; set; }
        public string ProjectCodes { get; set; }
        public string TM_DisplayName { get; set; }
        public string Com_Name { get; set; }

        //Search Filter

        public string SearchText { get; set; }
        public int PageNumber { get; set; }

        public int RowsOfPage { get; set; }



    }
}