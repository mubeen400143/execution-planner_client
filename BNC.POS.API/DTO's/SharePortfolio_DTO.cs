﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNC.POS.API.DTO_s
{
    public class SharePortfolio_DTO
    {
        public int id { get; set; }
        public int Portfolio_ID { get; set; }
        public string CompanyId { get; set; }
        public string EmployeeId { get; set; }
        public string Preference { get; set; }
        public bool IsActive { get; set; }
        public DateTime Shared_On { get; set; }
        public string Shared_By { get; set; }
        public DateTime Modified_On { get; set; }
        public string Modified_By { get; set; }
        public int FlagId { get; set; }
        public string Message { get; set; }
        public string Preferences { get; set; }
        public IEnumerable<PortfolioDTO> SelectedEmployees { get; set; }
        public string Shared_To { get; set; }


    }
}