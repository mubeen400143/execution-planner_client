﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNC.POS.API.DTO_s
{
    public class PortfolioDTO
    {
        public int Portfolio_ID { get; set; }
        public int Project_ID { get; set; }
        public string Emp_No { get; set; }
        public int id { get; set; }
        public int FlagId { get; set; }
        public string Portfolio_Name { get; set; }
        public string Project_Code { get; set; }
        public string Project_Name { get; set; }
        public string Project_Description { get; set; }
        public string Exec_BlockName { get; set; }
        public string Exec_BlockNo { get; set; }
        public string Project_Category { get; set; }
        public string User_Name { get; set; }
        public string User_ID { get; set; }
        public string Project_End_Date { get; set; }
        public string Status { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_DT { get; set; }
        public string Modified_By { get; set; }
        public DateTime Modified_DT { get; set; }

        public string Team_Autho { get; set; }
        public string Team_Coor { get; set; }
        public string Team_Informer { get; set; }
        public string Project_Auditor { get; set; }
        public string Remarks { get; set; }
        public string Team_Support { get; set; }
        public string Project_Owner { get; set; }
        public string DurationTime { get; set; }
        public string Emp_SystemRole { get; set; }
        public string SystemRole { get; set; }
        public string Com_Name { get; set; }
        public string TM_DisplayName { get; set; }
        public string SelectedProjectsJson { get; set; }
        public string JosnProjectsByPid { get; set; }
        public IEnumerable<PortfolioDTO> SelectedProjects { get; set; }

        // public IEnumerable<PortfolioDTO> JosnProjectsByPid { get; set; }

        public string message { get; set; }

        public DateTime DeadLine { get; set; }

        public string CompanyId { get; set; }

        public string EmployeeId { get; set; }
        public string Shared_To { get; set; }
        public string Pref { get; set; }
        public string Preferences { get; set; }

        public DateTime Shared_On { get; set; }

        public string Shared_By { get; set; }


        public string Project_Block { get; set; }
        public string PortfolioJson { get; set; }

        //public string JosnProjectsByPid { get; set; }
        public string SharedDetailsJson { get; set; }
        public string EmployeePreferenceJson { get; set; }
        public int PreferenceType { get; set; }

    }
}