﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNC.POS.API.DTO_s
{
    public class CompanyDTO
    {
        public string Com_No { get; set; }
        public string Com_Name { get; set; }

    }
}