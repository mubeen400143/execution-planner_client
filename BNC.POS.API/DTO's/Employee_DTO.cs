﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNC.POS.API.DTO_s
{
    public class Employee_DTO
    {
        public string Emp_No { get; set; }
        public string TM_DisplayName { get; set; }
        public string Emp_Name { get; set; }

        public string Emp_Second_Name { get; set; }

        public string Emp_Last_Name { get; set; }

        public string Position { get; set; }

        public string Emp_Comp_No { get; set; }

        public string Emp_Dept_No { get; set; }

        public string Emp_First_Name { get; set; }

        public int Portfolio_ID { get; set; }

    }
}