﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNC.POS.API.DTO_s
{
    public class LoginDTO
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
        public string Emp_No { get; set; }
        public string Emp_Comp_No { get; set; }
        public string Emp_SystemRole { get; set; }
        public DateTime Created_On { get; set; }
        public DateTime Expired_On { get; set; }
        public DateTime Loggedon { get; set; }
        public string Sales_Acc { get; set; }
        public bool Rejoin { get; set; }

        public DateTime DateOfJoin { get; set; }
        public string Emp_Email { get; set; }
        public string Emp_First_Name { get; set; }
        public string Emp_Last_Name  { get; set; }
        public string Emp_Second_Name { get; set; }
        public string Gender { get; set; }
        public string Emp_Dept_No { get; set; }

    }
}