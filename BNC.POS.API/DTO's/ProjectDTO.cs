﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNC.POS.API.DTO_s
{
    public class ProjectDTO
    {
        public string Exec_BlockNo { get; set; }
        public string Exec_BlockName { get; set; }

        public string Project_Code { get; set; }
        public string Project_Block { get; set; }
        public string Project_Name { get; set; }
        public string Project_Description { get; set; }

        public int Duration { get; set; }
        public DateTime DeadLine { get; set; }
        public string Status { get; set; }
        public string Emp_First_Name { get; set; }
        public string Emp_No { get; set; }
        public string Emp_Comp_No { get; set; }
        public string Team_Res { get; set; }

        public string Team_Autho { get; set; }
        public string Team_Coor { get; set; }
        public string Team_Informer { get; set; }

        public string Team_Support { get; set; }
        public string Project_Owner { get; set; }
        public string DurationTime { get; set; }
       
        public string Project_EndDate { get; set; }
        public int Project_Duration { get; set; }

        public string Remarks { get; set; }

        public int ProjectType_ID { get; set; }
        public int Project_ID { get; set; }

        public string ProjectType_Name { get; set; }

        public string message { get; set; }
        public string ProjectTypeJson { get; set; }
    }
}