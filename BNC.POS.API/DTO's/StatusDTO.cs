﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNC.POS.API.DTO_s
{
    public class StatusDTO
    {
        public int Portfolio_ID { get; set; }
        public string Status { get; set; }
    }
}