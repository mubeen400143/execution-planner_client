﻿using BNC.POS.API.BussinessHelpers;
using BNC.POS.API.DTO_s;
using BNC.POS.API.GenericHelpers;

using BNC.POS.API.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNC.POS.API.Services
{
    public class TestService: ITestService
    {
        private readonly ITestModel model;
        public TestService(TestModel _model)
        {
            model = _model;
        }

        public TestDTO InsertUpdateCountry(TestDTO obj)
        {
            try
            {
                var data = model.InsertUpdateCountry(obj);
                if (data.HasRows)
                {
                    while (data.Read())
                    {
                        obj.message = data["message"].ToString();
                    }
                }
                else
                    obj.message = "0";
            }
            catch (Exception ex)
            {
                obj.message = ex.ToString();
                throw;
            }
            return obj;
        }
        public TestDTO CountryById(TestDTO obj)
        {
            try
            {
                var data = model.GetCountryDetailsById(obj);
                if (data.HasRows)
                {
                    while (data.Read())
                    {
                        obj.Name = data["Name"].ToString();
                        obj.Id = int.Parse(data["Id"].ToString());
                    }
                }
                else
                    obj.message = "0";
            }
            catch (Exception ex)
            {
                obj.message = ex.ToString();
                throw;
            }
            return obj;
        }
        public IEnumerable<TestDTO> GetCountryList(TestDTO obj)
        {
            var data = model.GetCountryList(obj);
            var list = CustomDataReaderToGenericExtension.GetDataObjects<TestDTO>(data);
            data.Close();
            return list;
        }

        public IEnumerable<ProjectDTO> GetProjectTypes(ProjectDetails_DTO obj)
        {
            try
            {
                var data = model.GetProjectType(obj);

                var list = CustomDataReaderToGenericExtension.GetDataObjects<ProjectDTO>(data);
                data.Close();

                return list;
            }
            catch (Exception ex)
            {

                return null;
            }
          
        }

        public IEnumerable<PortfolioDTO> GetPortfolioByEmployee(PortfolioDTO obj)
        {
            try
            {
                var data = model.GetPortfolioByEmployee(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<PortfolioDTO>(data);
                data.Close();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IEnumerable<Employee_DTO> GetEmployeeByComp_No(Employee_DTO obj)
        {
            try
            {
                var data = model.GetEmployeeByComp_No(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<Employee_DTO>(data);
                data.Close();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public IEnumerable<ProjectDTO> GetProjects(ProjectDTO obj)
        {
            try
            {
                var data = model.GetProjects(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<ProjectDTO>(data);
                data.Close();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
          
        }
        public IEnumerable<ProjectDetails_DTO> GetProjectDetails()
        {
            try
            {
                var data = model.GetProjectDetails();
                var list = CustomDataReaderToGenericExtension.GetDataObjects<ProjectDetails_DTO>(data);
                data.Close();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
           
        }

        public IEnumerable<ProjectDetails_DTO> GetProjectDetailsByUserName(ProjectDetails_DTO obj)
        {
            try
            {
                var data = model.GetProjectDetailsByUserName(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<ProjectDetails_DTO>(data);
                data.Close();

                return list;

            }
            catch (Exception)
            {
                throw null;
            }
           
        }

        public IEnumerable<PortfolioDTO> GetProjectsBy_PortfolioId(PortfolioDTO obj)
        {
            try
            {
                var data = model.GetProjectsBy_PortfolioId(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<PortfolioDTO>(data);
                data.Close();
                return list;
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        public IEnumerable<Employee_DTO> GetEmployeById(Employee_DTO obj)
        {
            try
            {
                var data = model.GetEmployeById(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<Employee_DTO>(data);
                data.Close();

                return list;
            }
            catch (Exception)
            {

                return null;
            }
           
        }
        //GetEmployeById
        public IEnumerable<PortfolioDTO> GetPortfolio()
        {
            try
            {
                var data = model.GetPortfolio();
                var list = CustomDataReaderToGenericExtension.GetDataObjects<PortfolioDTO>(data);
                data.Close();

                return list;
            }
            catch (Exception)
            {

                return null;
            }

        }
        public IEnumerable<Employee_DTO> GetEmployees()
        {
            try
            {
                var data = model.GetEmployees();
                var list = CustomDataReaderToGenericExtension.GetDataObjects<Employee_DTO>(data);
                data.Close();

                return list;
            }
            catch (Exception)
            {

                return null;
            }
          
        }
        public IEnumerable<CompanyDTO> GetCompanies()
        {
            var data = model.GetCompanies();
            var list = CustomDataReaderToGenericExtension.GetDataObjects<CompanyDTO>(data);
            data.Close();

            return list;
        }

        public PortfolioDTO InsertUpdatePortfolio(PortfolioDTO obj)
        {
            try
            {
                var data = model.InsertUpdatePortfolio(obj);
                if (data.HasRows)
                {
                    while (data.Read())
                    {
                        obj.message = data["message"].ToString();
                    }
                    data.Close();
                }
              
                return obj;
            }
            catch (Exception ex)
            {
                obj.message = ex.Message.ToString();
                return obj;
            }
        }


        public SharePortfolio_DTO InsertUpdateSharePortfolio(SharePortfolio_DTO obj)
        {
            try
            {
                var data = model.InsertUpdateSharePortfolio(obj);
                if (data.HasRows)
                {
                    while (data.Read())
                    {
                        obj.Message = data["message"].ToString();
                    }
                    data.Close();
                }

                return obj;
            }
            catch (Exception ex)
            {
                obj.Message = ex.Message.ToString();
                return obj;
            }
        }


        public IEnumerable<LoginDTO> GetLoginDetails(LoginDTO obj)
        {
            try
            {
                var data = model.GetLoginDetails(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<LoginDTO>(data);
                data.Close();
                System.Data.SqlClient.SqlConnection.ClearAllPools();

                return list;
            }
            catch (Exception ex)
            {
                BusinessModelExceptionUtility.LogException(ex.Message, "TestService -> GetLoginDetails");
                return null;
            }

        }
        public IEnumerable<LoginDTO> GetUserDetails(LoginDTO obj)
        {
            try
            {
                var data = model.GetUserDetails(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<LoginDTO>(data);
                data.Close();
                System.Data.SqlClient.SqlConnection.ClearAllPools();

                return list;
            }
            catch (Exception ex)
            {
                BusinessModelExceptionUtility.LogException(ex.Message, "TestService -> GetUserDetails");
                return null;
            }

        }
        public IEnumerable<SharePortfolio_DTO> GetPreferencesByEmployeeId(SharePortfolio_DTO obj)
        {
            try
            {
                var data = model.GetPreferencesByEmployeeId(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<SharePortfolio_DTO>(data);
                data.Close();

                return list;
            }
            catch (Exception)
            {

                return null;
            }

        }
        public IEnumerable<SharePortfolio_DTO> GetShareDetailsByPortfolio_ID(SharePortfolio_DTO obj)
        {
            try
            {
                var data = model.GetShareDetailsByPortfolio_ID(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<SharePortfolio_DTO>(data);
                data.Close();

                return list;
            }
            catch (Exception)
            {

                return null;
            }

        }
        public IEnumerable<PortfolioDTO> GetPortfolioSharePreferences_Json(PortfolioDTO obj)
        {
            try
            {
                var data = model.GetPortfolioSharePreferences_Json(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<PortfolioDTO>(data);
                data.Close();

                return list;
            }
            catch (Exception ex)
            {

                return null;
            }

        }


        public IEnumerable<StatusDTO> GetStatus(StatusDTO obj)
        {
            try
            {
                var data = model.GetStatus(obj);
                var list = CustomDataReaderToGenericExtension.GetDataObjects<StatusDTO>(data);
                data.Close();

                return list;
            }
            catch (Exception)
            {

                return null;
            }

        }


    }

    public interface ITestService
    {
        TestDTO InsertUpdateCountry(TestDTO obj);
        TestDTO CountryById(TestDTO obj);
        IEnumerable<TestDTO> GetCountryList(TestDTO obj);
        IEnumerable<ProjectDTO> GetProjectTypes(ProjectDetails_DTO obj);

        IEnumerable<ProjectDTO> GetProjects(ProjectDTO obj);
        IEnumerable<PortfolioDTO> GetPortfolioByEmployee(PortfolioDTO obj);
        IEnumerable<ProjectDetails_DTO> GetProjectDetailsByUserName(ProjectDetails_DTO obj);
        IEnumerable<ProjectDetails_DTO> GetProjectDetails();
        IEnumerable<Employee_DTO> GetEmployeById(Employee_DTO obj);
        
        IEnumerable<Employee_DTO> GetEmployeeByComp_No(Employee_DTO obj);
        IEnumerable<Employee_DTO> GetEmployees();
        IEnumerable<PortfolioDTO> GetPortfolio();
        IEnumerable<CompanyDTO> GetCompanies();
        PortfolioDTO InsertUpdatePortfolio(PortfolioDTO obj);
        SharePortfolio_DTO InsertUpdateSharePortfolio(SharePortfolio_DTO obj);

        IEnumerable<LoginDTO> GetLoginDetails(LoginDTO obj);
        IEnumerable<LoginDTO> GetUserDetails(LoginDTO obj);
        IEnumerable<PortfolioDTO> GetProjectsBy_PortfolioId(PortfolioDTO obj);

        IEnumerable<SharePortfolio_DTO> GetPreferencesByEmployeeId(SharePortfolio_DTO obj);
        //GetShareDetailsByPortfolio_ID
        IEnumerable<SharePortfolio_DTO> GetShareDetailsByPortfolio_ID(SharePortfolio_DTO obj);
        IEnumerable<PortfolioDTO> GetPortfolioSharePreferences_Json(PortfolioDTO obj);

        IEnumerable<StatusDTO> GetStatus(StatusDTO obj);
    }
}