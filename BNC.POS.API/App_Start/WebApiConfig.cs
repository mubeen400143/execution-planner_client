﻿using BNC.POS.API.DependencyResolver;
using BNC.POS.API.Services;
using System.Web.Http.Cors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Unity;

namespace BNC.POS.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.EnableCors(new EnableCorsAttribute("http://localhost:4200,http://208.109.13.37,http://166.62.126.85", headers: "*", methods: "*"));
            // Web API configuration and services
            var container = new UnityContainer();

            container.RegisterType<ITestService, TestService>();

            config.DependencyResolver = new UnityResolver(container);
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
