﻿using BNC.POS.API.DTO_s;
using BNC.POS.API.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace BNC.POS.API.Controllers
{
    public class TestAPIController : ApiController
    {
        private readonly ITestService service;
        public TestAPIController(TestService _service)
        {
            service = _service;
        }
        /// <summary>
        /// Insert and Update Country
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public IHttpActionResult NewInsertUpdateAccounts(TestDTO obj)
        {
            var result = service.InsertUpdateCountry(obj);
            return Ok(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public IEnumerable<TestDTO> NewGetCountryList(TestDTO obj)
        {
            var list = service.GetCountryList(obj);
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public IHttpActionResult NewCountryById(TestDTO obj)
        {
            var result = service.CountryById(obj);
            return Ok(result);
        }

        public IEnumerable<ProjectDTO> NewGetProjectTypes(ProjectDetails_DTO obj)
        {
            var list = service.GetProjectTypes(obj);
            return list;
        }
        public IEnumerable<PortfolioDTO> NewGetPortfolioByEmployee(PortfolioDTO obj)
        {
            var list = service.GetPortfolioByEmployee(obj);
            return list;
        }
        public IEnumerable<ProjectDTO> NewGetProjects(ProjectDTO obj)
        {
            var list = service.GetProjects(obj);
            return list;
        }

        public IEnumerable<ProjectDetails_DTO> NewGetProjectsDetails()
        {
            var list = service.GetProjectDetails();
            return list;
        }
        public IEnumerable<Employee_DTO> NewGetEmployeById(Employee_DTO obj)
        {
            var list = service.GetEmployeById(obj);
            return list;
        }
        public IEnumerable<Employee_DTO> NewGetEmployeesByComp_No(Employee_DTO obj)
        {
            var list = service.GetEmployeeByComp_No(obj);
            return list;
        }

        public IEnumerable<ProjectDetails_DTO> NewGetProjectDetailsByUserName(ProjectDetails_DTO obj)
        {
            var list = service.GetProjectDetailsByUserName(obj);
            return list;
        }
        public IEnumerable<Employee_DTO> NewGetEmployees()
        {
            var list = service.GetEmployees();
            return list;
        }
        public IEnumerable<PortfolioDTO> NewGetPortfolio()
        {
            var list = service.GetPortfolio();
            return list;
        }

        public IEnumerable<CompanyDTO> NewGetCompanies()
        {
            var list = service.GetCompanies();
            return list;
        }

        [ResponseType(typeof(PortfolioDTO))]
        public IHttpActionResult NewInsertPortfolio(PortfolioDTO obj)
        {
            var json = JsonConvert.SerializeObject(obj.SelectedProjects);
            obj.SelectedProjectsJson = json;
            var result = service.InsertUpdatePortfolio(obj);
            return Ok(result);
        }

        public IHttpActionResult NewGetPortfolioSharePreferences_Json(PortfolioDTO obj)
        {
            
            var result = service.GetPortfolioSharePreferences_Json(obj);
            return Ok(result);
        }


        public IHttpActionResult NewGetProjectsBy_PortfolioId(PortfolioDTO obj)
        {
            var json = JsonConvert.SerializeObject(obj.JosnProjectsByPid);
             
            var result = service.GetProjectsBy_PortfolioId(obj);
            return Ok(result);
        }

        public IHttpActionResult NewInsertUpdateSharePortfolio(SharePortfolio_DTO obj)
        {
            var result = service.InsertUpdateSharePortfolio(obj);
            return Ok(result);
        }

        public IEnumerable<SharePortfolio_DTO> NewGetPreferencesByEmployeeId(SharePortfolio_DTO obj)
        {
            var list = service.GetPreferencesByEmployeeId(obj);
            return list;
        }
        public IEnumerable<SharePortfolio_DTO> NewGetShareDetailsByPortfolio_ID(SharePortfolio_DTO obj)
        {
            var list = service.GetShareDetailsByPortfolio_ID(obj);
            return list;
        }
        public IEnumerable<StatusDTO> NewGetStatus(StatusDTO obj)
        {
            var list = service.GetStatus(obj);
            return list;
        }
        public IEnumerable<LoginDTO> NewGetLoginDetails(LoginDTO obj)
        {
            try
            {
                var list = service.GetLoginDetails(obj);
                if (list != null)
                {
                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public IEnumerable<LoginDTO> NewGetUserDetails(LoginDTO obj)
        {
            try
            {
                var list = service.GetUserDetails(obj);
                if (list != null)
                {
                    return list;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}
